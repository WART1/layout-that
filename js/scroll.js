

// Quand on arrive à la partie 2, les sous-parties de la partie 1 disparaissent

$(window).scroll(function() {
  var $height = $(window).scrollTop();
  var p = $("#2o");
  var a = $("#cat2o");
  var position = p.position();
  var posit = a.position();
  var seuil = $height - position.top;
//console.log(seuil);



if (seuil > 0) {
  $('#cat1o1').css("display", "none");
  $('#cat1o1o1').css("display", "none");
  $('#cat1o1o2').css("display", "none");
  $('#cat1o2').css("display", "none");
  $('#cat1o2o1').css("display", "none");
  $('#cat1o2o2').css("display", "none");
  $('#cat1o3').css("display", "none");
  $('#cat1o3o1').css("display", "none");
  $('#cat1o3o2').css("display", "none");
  $('#cat1o3o3').css("display", "none");
  $('#introd').css("display", "none");
  $('#cat1o').css("display", "none");
}

if (seuil < 0) {
  $('#cat1o1').css("display", "");
  $('#cat1o1o1').css("display", "");
  $('#cat1o1o2').css("display", "");
  $('#cat1o2').css("display", "");
  $('#cat1o2o1').css("display", "");
  $('#cat1o2o2').css("display", "");
  $('#cat1o3').css("display", "");
  $('#cat1o3o1').css("display", "");
  $('#cat1o3o2').css("display", "");
  $('#cat1o3o3').css("display", "");
  $('#introd').css("display", "");
  $('#cat1o').css("display", "");
}



  });

// Quand on arrive à la partie 3, les sous-parties de la partie 2 disparaissent
  $(window).scroll(function() {
    var $height = $(window).scrollTop();
    var p = $("#3o");
    var a = $("#cat3o");
    var position = p.position();
    var posit = a.position();
    var seuil = $height - position.top;
  console.log(seuil);


  if (seuil > 0) {
    $('#cat2o').css("display", "none");
    $('#cat2o1').css("display", "none");
    $('#cat2o1o1').css("display", "none");
    $('#cat2o1o2').css("display", "none");
    $('#cat2o2').css("display", "none");
    $('#cat2o2o1').css("display", "none");
    $('#cat2o2o2').css("display", "none");
    $('#cat2o2o3').css("display", "none");
  }

  if (seuil < 0) {
    $('#cat2o').css("display", "");
    $('#cat2o1').css("display", "");
    $('#cat2o1o1').css("display", "");
    $('#cat2o1o2').css("display", "");
    $('#cat2o2').css("display", "");
    $('#cat2o2o1').css("display", "");
    $('#cat2o2o2').css("display", "");
    $('#cat2o2o3').css("display", "");
  }

    });
