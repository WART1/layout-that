


$(window).scroll(function() {
  /*
    var p = $( "#1o" );
  var position = p.position();
  console.log(position);

  */
  // introduction
  var $height = $(window).scrollTop();
  var p = $("#intro");
  var position = p.position();
  var seuil = $height - position.top;

  if (seuil > 0) {
    $("#introd").css("-webkit-background-clip", "text");
    $("#introd").css("-webkit-text-fill-color", "transparent");
    $("#introd").css("-webkit-text-stroke", ".8px blue");
  }


  // 1.
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#1o");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#introd").css("-webkit-background-clip", "");
    $("#introd").css("-webkit-text-fill-color", "");
    $("#introd").css("-webkit-text-stroke", "");

    $("#cat1o").css("-webkit-background-clip", "text");
    $("#cat1o").css("-webkit-text-fill-color", "transparent");
    $("#cat1o").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat1o").css("-webkit-background-clip", "");
    $("#cat1o").css("-webkit-text-fill-color", "");
    $("#cat1o").css("-webkit-text-stroke", "");

  }

  // 1.1
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#1o1");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat1o").css("-webkit-background-clip", "");
    $("#cat1o").css("-webkit-text-fill-color", "");
    $("#cat1o").css("-webkit-text-stroke", "");

    $("#cat1o1").css("-webkit-background-clip", "text");
    $("#cat1o1").css("-webkit-text-fill-color", "transparent");
    $("#cat1o1").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat1o1").css("-webkit-background-clip", "");
    $("#cat1o1").css("-webkit-text-fill-color", "");
    $("#cat1o1").css("-webkit-text-stroke", "");

  }

  // 1.1.1
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#1o1o1");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat1o1").css("-webkit-background-clip", "");
    $("#cat1o1").css("-webkit-text-fill-color", "");
    $("#cat1o1").css("-webkit-text-stroke", "");

    $("#cat1o1o1").css("-webkit-background-clip", "text");
    $("#cat1o1o1").css("-webkit-text-fill-color", "transparent");
    $("#cat1o1o1").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat1o1o1").css("-webkit-background-clip", "");
    $("#cat1o1o1").css("-webkit-text-fill-color", "");
    $("#cat1o1o1").css("-webkit-text-stroke", "");

  }

  // 1.1.2
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#1o1o2");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat1o1o1").css("-webkit-background-clip", "");
    $("#cat1o1o1").css("-webkit-text-fill-color", "");
    $("#cat1o1o1").css("-webkit-text-stroke", "");

    $("#cat1o1o2").css("-webkit-background-clip", "text");
    $("#cat1o1o2").css("-webkit-text-fill-color", "transparent");
    $("#cat1o1o2").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat1o1o2").css("-webkit-background-clip", "");
    $("#cat1o1o2").css("-webkit-text-fill-color", "");
    $("#cat1o1o2").css("-webkit-text-stroke", "");

  }

  // 1.2
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#1o2");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat1o1o2").css("-webkit-background-clip", "");
    $("#cat1o1o2").css("-webkit-text-fill-color", "");
    $("#cat1o1o2").css("-webkit-text-stroke", "");

    $("#cat1o2").css("-webkit-background-clip", "text");
    $("#cat1o2").css("-webkit-text-fill-color", "transparent");
    $("#cat1o2").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat1o2").css("-webkit-background-clip", "");
    $("#cat1o2").css("-webkit-text-fill-color", "");
    $("#cat1o2").css("-webkit-text-stroke", "");

  }

  // 1.2.1
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#1o2o1");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat1o2").css("-webkit-background-clip", "");
    $("#cat1o2").css("-webkit-text-fill-color", "");
    $("#cat1o2").css("-webkit-text-stroke", "");

    $("#cat1o2o1").css("-webkit-background-clip", "text");
    $("#cat1o2o1").css("-webkit-text-fill-color", "transparent");
    $("#cat1o2o1").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat1o2o1").css("-webkit-background-clip", "");
    $("#cat1o2o1").css("-webkit-text-fill-color", "");
    $("#cat1o2o1").css("-webkit-text-stroke", "");

  }

  // 1.2.2
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#1o2o2");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat1o2o1").css("-webkit-background-clip", "");
    $("#cat1o2o1").css("-webkit-text-fill-color", "");
    $("#cat1o2o1").css("-webkit-text-stroke", "");

    $("#cat1o2o2").css("-webkit-background-clip", "text");
    $("#cat1o2o2").css("-webkit-text-fill-color", "transparent");
    $("#cat1o2o2").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat1o2o2").css("-webkit-background-clip", "");
    $("#cat1o2o2").css("-webkit-text-fill-color", "");
    $("#cat1o2o2").css("-webkit-text-stroke", "");

  }

  // 1.3
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#1o3");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat1o2o2").css("-webkit-background-clip", "");
    $("#cat1o2o2").css("-webkit-text-fill-color", "");
    $("#cat1o2o2").css("-webkit-text-stroke", "");

    $("#cat1o3").css("-webkit-background-clip", "text");
    $("#cat1o3").css("-webkit-text-fill-color", "transparent");
    $("#cat1o3").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat1o3").css("-webkit-background-clip", "");
    $("#cat1o3").css("-webkit-text-fill-color", "");
    $("#cat1o3").css("-webkit-text-stroke", "");

  }

  // 1.3.1
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#1o3o1");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat1o3").css("-webkit-background-clip", "");
    $("#cat1o3").css("-webkit-text-fill-color", "");
    $("#cat1o3").css("-webkit-text-stroke", "");

    $("#cat1o3o1").css("-webkit-background-clip", "text");
    $("#cat1o3o1").css("-webkit-text-fill-color", "transparent");
    $("#cat1o3o1").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat1o3o1").css("-webkit-background-clip", "");
    $("#cat1o3o1").css("-webkit-text-fill-color", "");
    $("#cat1o3o1").css("-webkit-text-stroke", "");

  }

  // 1.3.2
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#1o3o2");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat1o3o1").css("-webkit-background-clip", "");
    $("#cat1o3o1").css("-webkit-text-fill-color", "");
    $("#cat1o3o1").css("-webkit-text-stroke", "");

    $("#cat1o3o2").css("-webkit-background-clip", "text");
    $("#cat1o3o2").css("-webkit-text-fill-color", "transparent");
    $("#cat1o3o2").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat1o3o2").css("-webkit-background-clip", "");
    $("#cat1o3o2").css("-webkit-text-fill-color", "");
    $("#cat1o3o2").css("-webkit-text-stroke", "");
  }

  // 1.3.3
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#1o3o3");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat1o3o2").css("-webkit-background-clip", "");
    $("#cat1o3o2").css("-webkit-text-fill-color", "");
    $("#cat1o3o2").css("-webkit-text-stroke", "");

    $("#cat1o3o3").css("-webkit-background-clip", "text");
    $("#cat1o3o3").css("-webkit-text-fill-color", "transparent");
    $("#cat1o3o3").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat1o3o3").css("-webkit-background-clip", "");
    $("#cat1o3o3").css("-webkit-text-fill-color", "");
    $("#cat1o3o3").css("-webkit-text-stroke", "");
  }

  // 2.
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#2o");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat1o3o3").css("-webkit-background-clip", "");
    $("#cat1o3o3").css("-webkit-text-fill-color", "");
    $("#cat1o3o3").css("-webkit-text-stroke", "");

    $("#cat2o").css("-webkit-background-clip", "text");
    $("#cat2o").css("-webkit-text-fill-color", "transparent");
    $("#cat2o").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat2o").css("-webkit-background-clip", "");
    $("#cat2o").css("-webkit-text-fill-color", "");
    $("#cat2o").css("-webkit-text-stroke", "");
  }

  // 2.1
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#2o1");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat2o").css("-webkit-background-clip", "");
    $("#cat2o").css("-webkit-text-fill-color", "");
    $("#cat2o").css("-webkit-text-stroke", "");

    $("#cat2o1").css("-webkit-background-clip", "text");
    $("#cat2o1").css("-webkit-text-fill-color", "transparent");
    $("#cat2o1").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat2o1").css("-webkit-background-clip", "");
    $("#cat2o1").css("-webkit-text-fill-color", "");
    $("#cat2o1").css("-webkit-text-stroke", "");
  }

  // 2.1.1
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#2o1o1");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat2o1").css("-webkit-background-clip", "");
    $("#cat2o1").css("-webkit-text-fill-color", "");
    $("#cat2o1").css("-webkit-text-stroke", "");

    $("#cat2o1o1").css("-webkit-background-clip", "text");
    $("#cat2o1o1").css("-webkit-text-fill-color", "transparent");
    $("#cat2o1o1").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat2o1o1").css("-webkit-background-clip", "");
    $("#cat2o1o1").css("-webkit-text-fill-color", "");
    $("#cat2o1o1").css("-webkit-text-stroke", "");
  }

  // 2.1.2
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#2o1o2");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat2o1o1").css("-webkit-background-clip", "");
    $("#cat2o1o1").css("-webkit-text-fill-color", "");
    $("#cat2o1o1").css("-webkit-text-stroke", "");

    $("#cat2o1o2").css("-webkit-background-clip", "text");
    $("#cat2o1o2").css("-webkit-text-fill-color", "transparent");
    $("#cat2o1o2").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat2o1o2").css("-webkit-background-clip", "");
    $("#cat2o1o2").css("-webkit-text-fill-color", "");
    $("#cat2o1o2").css("-webkit-text-stroke", "");
  }

  // 2.2
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#2o2");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat2o1o2").css("-webkit-background-clip", "");
    $("#cat2o1o2").css("-webkit-text-fill-color", "");
    $("#cat2o1o2").css("-webkit-text-stroke", "");

    $("#cat2o2").css("-webkit-background-clip", "text");
    $("#cat2o2").css("-webkit-text-fill-color", "transparent");
    $("#cat2o2").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat2o2").css("-webkit-background-clip", "");
    $("#cat2o2").css("-webkit-text-fill-color", "");
    $("#cat2o2").css("-webkit-text-stroke", "");
  }

  // 2.2.1
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#2o2o1");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat2o2").css("-webkit-background-clip", "");
    $("#cat2o2").css("-webkit-text-fill-color", "");
    $("#cat2o2").css("-webkit-text-stroke", "");

    $("#cat2o2o1").css("-webkit-background-clip", "text");
    $("#cat2o2o1").css("-webkit-text-fill-color", "transparent");
    $("#cat2o2o1").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat2o2o1").css("-webkit-background-clip", "");
    $("#cat2o2o1").css("-webkit-text-fill-color", "");
    $("#cat2o2o1").css("-webkit-text-stroke", "");
  }


  // 2.2.2
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#2o2o2");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat2o2o1").css("-webkit-background-clip", "");
    $("#cat2o2o1").css("-webkit-text-fill-color", "");
    $("#cat2o2o1").css("-webkit-text-stroke", "");

    $("#cat2o2o2").css("-webkit-background-clip", "text");
    $("#cat2o2o2").css("-webkit-text-fill-color", "transparent");
    $("#cat2o2o2").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat2o2o2").css("-webkit-background-clip", "");
    $("#cat2o2o2").css("-webkit-text-fill-color", "");
    $("#cat2o2o2").css("-webkit-text-stroke", "");
  }

  // 2.2.3
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#2o2o3");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat2o2o2").css("-webkit-background-clip", "");
    $("#cat2o2o2").css("-webkit-text-fill-color", "");
    $("#cat2o2o2").css("-webkit-text-stroke", "");

    $("#cat2o2o3").css("-webkit-background-clip", "text");
    $("#cat2o2o3").css("-webkit-text-fill-color", "transparent");
    $("#cat2o2o3").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat2o2o3").css("-webkit-background-clip", "");
    $("#cat2o2o3").css("-webkit-text-fill-color", "");
    $("#cat2o2o3").css("-webkit-text-stroke", "");
  }

  // 3.
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#3o");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat2o2o3").css("-webkit-background-clip", "");
    $("#cat2o2o3").css("-webkit-text-fill-color", "");
    $("#cat2o2o3").css("-webkit-text-stroke", "");

    $("#cat3o").css("-webkit-background-clip", "text");
    $("#cat3o").css("-webkit-text-fill-color", "transparent");
    $("#cat3o").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat3o").css("-webkit-background-clip", "");
    $("#cat3o").css("-webkit-text-fill-color", "");
    $("#cat3o").css("-webkit-text-stroke", "");
  }

  // 3.1
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#3o1");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat3o").css("-webkit-background-clip", "");
    $("#cat3o").css("-webkit-text-fill-color", "");
    $("#cat3o").css("-webkit-text-stroke", "");

    $("#cat3o1").css("-webkit-background-clip", "text");
    $("#cat3o1").css("-webkit-text-fill-color", "transparent");
    $("#cat3o1").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat3o1").css("-webkit-background-clip", "");
    $("#cat3o1").css("-webkit-text-fill-color", "");
    $("#cat3o1").css("-webkit-text-stroke", "");
  }

  // 3.1.1
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#3o1o1");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat3o1").css("-webkit-background-clip", "");
    $("#cat3o1").css("-webkit-text-fill-color", "");
    $("#cat3o1").css("-webkit-text-stroke", "");

    $("#cat3o1o1").css("-webkit-background-clip", "text");
    $("#cat3o1o1").css("-webkit-text-fill-color", "transparent");
    $("#cat3o1o1").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat3o1o1").css("-webkit-background-clip", "");
    $("#cat3o1o1").css("-webkit-text-fill-color", "");
    $("#cat3o1o1").css("-webkit-text-stroke", "");
  }


  // 3.1.2
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#3o1o2");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat3o1o1").css("-webkit-background-clip", "");
    $("#cat3o1o1").css("-webkit-text-fill-color", "");
    $("#cat3o1o1").css("-webkit-text-stroke", "");

    $("#cat3o1o2").css("-webkit-background-clip", "text");
    $("#cat3o1o2").css("-webkit-text-fill-color", "transparent");
    $("#cat3o1o2").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat3o1o2").css("-webkit-background-clip", "");
    $("#cat3o1o2").css("-webkit-text-fill-color", "");
    $("#cat3o1o2").css("-webkit-text-stroke", "");
  }

  // 3.2
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#3o2");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat3o1o2").css("-webkit-background-clip", "");
    $("#cat3o1o2").css("-webkit-text-fill-color", "");
    $("#cat3o1o2").css("-webkit-text-stroke", "");

    $("#cat3o2").css("-webkit-background-clip", "text");
    $("#cat3o2").css("-webkit-text-fill-color", "transparent");
    $("#cat3o2").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat3o2").css("-webkit-background-clip", "");
    $("#cat3o2").css("-webkit-text-fill-color", "");
    $("#cat3o2").css("-webkit-text-stroke", "");
  }

  // 3.2.1
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#3o2o1");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat3o2").css("-webkit-background-clip", "");
    $("#cat3o2").css("-webkit-text-fill-color", "");
    $("#cat3o2").css("-webkit-text-stroke", "");

    $("#cat3o2o1").css("-webkit-background-clip", "text");
    $("#cat3o2o1").css("-webkit-text-fill-color", "transparent");
    $("#cat3o2o1").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat3o2o1").css("-webkit-background-clip", "");
    $("#cat3o2o1").css("-webkit-text-fill-color", "");
    $("#cat3o2o1").css("-webkit-text-stroke", "");
  }

  // 3.2.2
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#3o2o2");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat3o2o1").css("-webkit-background-clip", "");
    $("#cat3o2o1").css("-webkit-text-fill-color", "");
    $("#cat3o2o1").css("-webkit-text-stroke", "");

    $("#cat3o2o2").css("-webkit-background-clip", "text");
    $("#cat3o2o2").css("-webkit-text-fill-color", "transparent");
    $("#cat3o2o2").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat3o2o2").css("-webkit-background-clip", "");
    $("#cat3o2o2").css("-webkit-text-fill-color", "");
    $("#cat3o2o2").css("-webkit-text-stroke", "");
  }


  // 3.2.3
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#3o2o3");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat3o2o2").css("-webkit-background-clip", "");
    $("#cat3o2o2").css("-webkit-text-fill-color", "");
    $("#cat3o2o2").css("-webkit-text-stroke", "");

    $("#cat3o2o3").css("-webkit-background-clip", "text");
    $("#cat3o2o3").css("-webkit-text-fill-color", "transparent");
    $("#cat3o2o3").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat3o2o3").css("-webkit-background-clip", "");
    $("#cat3o2o3").css("-webkit-text-fill-color", "");
    $("#cat3o2o3").css("-webkit-text-stroke", "");
  }

  // 3.3
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#3o3");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat3o2o3").css("-webkit-background-clip", "");
    $("#cat3o2o3").css("-webkit-text-fill-color", "");
    $("#cat3o2o3").css("-webkit-text-stroke", "");

    $("#cat3o3").css("-webkit-background-clip", "text");
    $("#cat3o3").css("-webkit-text-fill-color", "transparent");
    $("#cat3o3").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat3o3").css("-webkit-background-clip", "");
    $("#cat3o3").css("-webkit-text-fill-color", "");
    $("#cat3o3").css("-webkit-text-stroke", "");
  }

  // 3.3.1
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#3o3o1");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat3o3").css("-webkit-background-clip", "");
    $("#cat3o3").css("-webkit-text-fill-color", "");
    $("#cat3o3").css("-webkit-text-stroke", "");

    $("#cat3o3o1").css("-webkit-background-clip", "text");
    $("#cat3o3o1").css("-webkit-text-fill-color", "transparent");
    $("#cat3o3o1").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat3o3o1").css("-webkit-background-clip", "");
    $("#cat3o3o1").css("-webkit-text-fill-color", "");
    $("#cat3o3o1").css("-webkit-text-stroke", "");
  }

  // 3.3.2
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#3o3o2");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat3o3o1").css("-webkit-background-clip", "");
    $("#cat3o3o1").css("-webkit-text-fill-color", "");
    $("#cat3o3o1").css("-webkit-text-stroke", "");

    $("#cat3o3o2").css("-webkit-background-clip", "text");
    $("#cat3o3o2").css("-webkit-text-fill-color", "transparent");
    $("#cat3o3o2").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#cat3o3o2").css("-webkit-background-clip", "");
    $("#cat3o3o2").css("-webkit-text-fill-color", "");
    $("#cat3o3o2").css("-webkit-text-stroke", "");
  }

  // conclusion
  // stocker dans la variable $height le scroll de l'écran
  var $height = $(window).scrollTop();
  // stocker dans la variable p la position de l'id #1o
  var p = $("#cclu");
  var position = p.position();
  // stocker dans la variable "seuil" le résultat du scroll dans l'écran moins la position sur la page de l'élement
  var seuil = $height - position.top;

  // si "seuil" est supérieur à 0 on change dans le css la coleur de l'élément "#cat01" en rouge
  if (seuil > 0) {
    $("#cat3o3o2").css("-webkit-background-clip", "");
    $("#cat3o3o2").css("-webkit-text-fill-color", "");
    $("#cat3o3o2").css("-webkit-text-stroke", "");

    $("#ccl").css("-webkit-background-clip", "text");
    $("#ccl").css("-webkit-text-fill-color", "transparent");
    $("#ccl").css("-webkit-text-stroke", ".8px blue");

  }
  if (seuil < 0) {
    $("#ccl").css("-webkit-background-clip", "");
    $("#ccl").css("-webkit-text-fill-color", "");
    $("#ccl").css("-webkit-text-stroke", "");
  }

});
