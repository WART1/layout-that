# [REHAP](1234567890azertyuiopqsdfghjklmwxcvbn.fr)

[![PyPI](https://img.shields.io/pypi/l/fsfe-reuse.svg)](https://www.gnu.org/licenses/gpl-3.0.html)
[![reuse compliant](https://img.shields.io/badge/reuse-compliant-green.svg)](https://git.fsfe.org/reuse/reuse)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)



![](/Users/utilisateur/bitBucket/layout-that/medias/33.jpg)

---

## Introduction

A repository wich contains my 1234567890azertyuiopqsdfghjklmwxcvbn.fr website.
Printable part made with bindery.js
[Bindery.js](https://evanbrooks.info/bindery/guide/) for formating and exporting web content to print.

## Contents

* All html, css & js files.
* [You can view a working site of this repository.](http://www.1234567890azertyuiopqsdfghjklmwxcvbn.fr/)

## Install Bindery

You can simply download all the files found in this repository. The link(s) found in index.html refer to an example of Bindery.js.
The basic setup has the following code within the body. The .content selector is a tag that englobes the web content to include.

```javascript
 <script>
    Bindery.makeBook({
      content: {
        url: 'pages/content.html',
        selector: ".content",
      },
    });
  </script>
```

[You can read and indeed print an online guide over here.](https://evanbrooks.info/bindery/book/)

## Contact & Sundries

* martinodek@gmail.com
* Version v0.1
* Tools used : HTML & CSS & JS

## Contribute
Fork it !

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

For more information https://www.gnu.org/licenses/gpl-3.0.en.html

The program in this repository meet the requirements to be REUSE compliant,
meaning its license and copyright is expressed in such as way so that it
can be read by both humans and computers alike.

For more information, see https://reuse.software/
